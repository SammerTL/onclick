package com.example.exercise4android

import android.companion.CompanionDeviceManager
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AbsListView
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.view.menu.ActionMenuItemView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.my_listitem.view.*


class MyAdapter(val context: Context,
              val items:ArrayList<Myitem>, val listener: MyListener
) : RecyclerView.Adapter<MyAdapter.MyViewHolder>() {

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val title: TextView = itemView.my_listitem_title
        val description: TextView = itemView.my_listitem_description
    }
    override fun onCreateViewHolder(parent: ViewGroup,viewType: Int ):MyViewHolder{
        val view = LayoutInflater.from(context).inflate(R.layout.my_listitem, parent,  false)
        return MyViewHolder(view)
    }
    override fun getItemCount() : Int = items.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int)
    {
        val item = items[position]
        holder.title.text = item.title
        holder.description.text = item.description
        holder.itemView.setOnClickListener{
            listener.onItemClick(position)
        }
    }

}

