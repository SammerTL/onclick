package com.example.exercise4android

import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*
import java.security.AccessController.getContext

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val content = ArrayList<Myitem>()

        //val myviewlist = findViewById<RecyclerView>(R.id.mylist)
        val verticallist = LinearLayoutManager(this)
        content.add(Myitem("Willem", "dit is Willem jan henk"))
        content.add(Myitem("Peter", "dit is Peter jan willem"))
        content.add(Myitem("Klaas", "dit is Klaas jan huntelaar"))
        content.add(Myitem("Frederik", "dit is Frederik hendrik III"))
        content.add(Myitem("Rohan", "dit is een rider of Rohan"))
        content.add(Myitem("Jordy", "dit is Jordan Ville"))
        content.add(Myitem("Jesse", "dit is J3sse"))
        content.add(Myitem("Wessel", "dit is Sinoxity"))



        val list = MyAdapter(this, content, clickerface)
        setContentView(R.layout.activity_main)
        mylist.adapter = list
        mylist.layoutManager = verticallist

        

    }
    val clickerface =  object:MyListener
    {
        override fun onItemClick(position: Int)
        {
            val intent : Intent = Intent(this@MainActivity, DetailsActivity::class.java)
            intent.putExtra("INTENT_USER_ID", position)
            startActivity(intent)
        }

    }
}
