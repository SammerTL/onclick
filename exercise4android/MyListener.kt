package com.example.exercise4android

interface MyListener {
    fun onItemClick(position: Int)
}